﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InertieVaisseau : MonoBehaviour
{
    public float speed = 10.0f;
    public Rigidbody2D rgbd;

    public Transform firepoint;

    public float rotationspeed = 3 , test;

    public KeyCode propulsion;
    // Start is called before the first frame update
    void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
        test = rgbd.velocity.y;
    }

    // Update is called once per frame
    void Update()
    {
        float xaxis = Input.GetAxis("Horizontal");
        if (gamecontroller.stop)
        {
            if (Input.GetKey(propulsion) && rgbd.velocity.y <=5)
            { 
                speed = 5f;
                Thrust(speed);

            }
            if (Input.GetKey(propulsion) && rgbd.velocity.y >=4)
            {

                speed = 5f;
                Thrust(speed);

            }
        }
        
        //Rotate(transform,xaxis * -rotationspeed );
    }

  /*  public void Rotate(Transform t, float amount)
    {
        
        t.Rotate(0, 0, amount);
    }*/

    public void Thrust(float amount)
    {
        Vector2 force = transform.up * amount;
        rgbd.AddForce(force);
    }
    
}
