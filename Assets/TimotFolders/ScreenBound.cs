﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenBound : MonoBehaviour
{
    private Vector2 screenbounds;
    public Camera MainCamera;
    private float objectWidth;
    private float objectHeight;
    // Start is called before the first frame update
    void Start()
    { // On récupère la taille du sprite et on recupère le vector2 de la Caméra
        screenbounds = MainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, MainCamera.transform.position.z));
        objectWidth = transform.GetComponent<SpriteRenderer>().bounds.extents.x /2;
        objectHeight = transform.GetComponent<SpriteRenderer>().bounds.extents.y/2;
    }

    // Update is called once per frame
    void LateUpdate()
    {// Permet de faire en sorte que l'objectwidht et l'objectHeight de sorte pas de l'écran
        Vector3 viewpos = transform.position;
        viewpos.x = Mathf.Clamp(viewpos.x, screenbounds.x * -1 + objectWidth, screenbounds.x  - objectWidth);
        viewpos.y = Mathf.Clamp(viewpos.y, screenbounds.y * -1 + objectHeight, screenbounds.y  - objectHeight);
        transform.position = viewpos;
    }
}
