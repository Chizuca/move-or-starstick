﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gamecontroller : MonoBehaviour
{
    public GameObject gameOverPanel , finishpanel , finishpane2;

    public PlayerHealth player1win, player2win;

    public bool manager;

    public TextMeshProUGUI scorePlayer1, scorePlayer2;

    public GameObject player1, player2;
    public int scoreupdateplayer1, scoreupdateplayer2 = 1;
    public int score;
    public bool party;


    public static bool stop = true , addmalus;

    public InertieVaisseau rgbdPL1, rgbdPL2;

    public Transform spawnplayer1, spawnplayer2;
    
    public TextMeshProUGUI décompte;
    public float countdown;
    public int timer;

    private bool t = false;
    public float timeur;

    public TextMeshProUGUI winnertext, nextroundtext, décompteText;
    
    public TMP_ColorGradient blue, red;
    

    public Sprite[] emote;

    public Image emotplayer1, emotplayer2;
    public MalusScript malusscript;
    public bool onmalus =true;

    public string reload;


    [Header("End game")] 
    public GameObject panelend;
    public TextMeshProUGUI txt_end;
    public Sprite[] win_sprites;
    public Image win_img;
    
    // Start is called before the first frame update
    void Start()
    {
       FindObjectOfType<AudioManager>().Play("MainAudio");
       BOUNCINESS.Bouciness = false;
       RotatePlayer.malusSwitch = false;
       thunderMalus.thunder = false;
       Time.timeScale = 1f;
        stop = true;
       
    }

    // Update is called once per frame
    void Update()
    {
        timer = Mathf.RoundToInt(countdown);
        décompte.text = (timer + "") ;
        if (manager == true && party)
        {
            StartCoroutine(GameSetup());
            party = false;
        }

        if (t == true)
        {//Décompte
            countdown -= Time.deltaTime;
        }

        
        
            if (score == 5 && onmalus && !addmalus && countdown <= 0 || score == 10 && onmalus && !addmalus && countdown <= 0 || score == 15 && onmalus && !addmalus && countdown <= 0) 
            {//Appel le malus panel tout les 5 tours
                GameObject.Find("Canvas").GetComponent<Canvas>().gameObject.GetComponent<MalusScript>().oneTryBis = true;
                onmalus = false;
                addmalus = true;
                

            }

            if (score!= 5 || score!= 10 || score!= 15)
            {
                onmalus = true;
            }
            
            

        if (scoreupdateplayer1 == 11 || scoreupdateplayer2 == 11)
        {// Reset les var Score et Score des players
            timeur += Time.deltaTime;
           /* if (scoreupdateplayer1 == 16)
            {
                finishpanel.SetActive(true);
            }
            else if (scoreupdateplayer2 == 16)
            {
                finishpane2.SetActive(true);
            }*/
            if (timeur > 3)
            {
                if (scoreupdateplayer1 > scoreupdateplayer2)
                {
                    StartCoroutine(EndPlayer1Win());
                    
                }

               else if (scoreupdateplayer2 > scoreupdateplayer1)
                {
                    StartCoroutine(EndPlayer2Win());
                }
            }
            
        }
        
    }

    IEnumerator EndPlayer1Win()
    {
        panelend.SetActive(true);
        txt_end.text = "Player 1 win the game !!!";
        win_img.sprite = win_sprites[1];
        yield return new WaitForSeconds(3.5f);
        SceneManager.LoadScene(reload, LoadSceneMode.Single);
        timeur = 0;
        yield break;
    }

    IEnumerator EndPlayer2Win()
    {
        panelend.SetActive(true);
        txt_end.text = "Player 2 win the game !!!";
        win_img.sprite = win_sprites[0];
        yield return new WaitForSeconds(3.5f);
        SceneManager.LoadScene(reload, LoadSceneMode.Single);
        timeur = 0;
        yield break;
    }
   

    IEnumerator GameSetup()
    {// Augmente le score du player qui viens de gagner et reset toute les valeurs
        score++;
        yield return new WaitForSeconds(1.0f);
        gameOverPanel.SetActive(true);
        stop = false;
        player1.transform.position = spawnplayer1.transform.position;
        player2.transform.position = spawnplayer2.transform.position;
        rgbdPL1.rgbd.velocity = new Vector2(0, 0);
        rgbdPL2.rgbd.velocity = new Vector2(0, 0);
        rgbdPL1.rgbd.simulated = false;
        rgbdPL2.rgbd.simulated = false;
        if (player1.activeSelf)
        {
            scoreupdateplayer1++;
            scorePlayer1.text = scoreupdateplayer1.ToString();
            winnertext.colorGradientPreset = blue;
            nextroundtext.colorGradientPreset = blue;
            décompteText.colorGradientPreset = blue;
            emotplayer1.sprite = emote[2];
        }
        else if (player1.activeSelf == false)
        {
             emotplayer1.sprite = emote[3];
        }
        if (player2.activeSelf)
        {
            scoreupdateplayer2++;
            scorePlayer2.text = scoreupdateplayer2.ToString();
            winnertext.colorGradientPreset = red;
            nextroundtext.colorGradientPreset = red;
            décompteText.colorGradientPreset = red;
            emotplayer2.sprite = emote[1];
        }
        else if (player2.activeSelf == false)
        {
            emotplayer2.sprite = emote[0];
        }
        Debug.Log("test");
        t = true;
        yield return new WaitForSeconds(4.0F);
        t = false;
        countdown = 4;
        stop = true;
        rgbdPL1.rgbd.simulated = true;
        rgbdPL2.rgbd.simulated = true;
        player1.SetActive(true);
        player2.SetActive(true);
        gameOverPanel.SetActive(false);
        player1win.SetMaxHealth(player1win.maxHealth);
        player1win.currenthealth = player1win.maxHealth;
        player2win.SetMaxHealth(player2win.maxHealth);
        player2win.currenthealth = player2win.maxHealth;
        
    }
}
