﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BOUNCINESS : MonoBehaviour
{
    public static bool Bouciness = false;

    public CompositeCollider2D camérabounds;
    public PhysicsMaterial2D bouncs;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Bouciness)
        {
            bouncs.bounciness = 1f;
            camérabounds.sharedMaterial = bouncs;

        }
        else if (Bouciness == false)
        {
            bouncs.bounciness = 0.2f;
            camérabounds.sharedMaterial = bouncs;
        }

    }

}
