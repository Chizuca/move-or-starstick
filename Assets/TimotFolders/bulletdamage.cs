﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class bulletdamage : MonoBehaviour
{
    public PlayerHealth playerhealth;

    public string player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D other)
    {// Inflige des dégats au player2
        if (other.gameObject.CompareTag(player))
        {
            playerhealth.TakeDamage(10);
            gameObject.SetActive(false);
            
        }
    }

    public void OnBecameInvisible()
    {
        gameObject.SetActive(false);
    }
}
