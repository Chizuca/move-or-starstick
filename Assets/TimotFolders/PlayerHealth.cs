﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class PlayerHealth : MonoBehaviour
{
    public Slider slider;

    public int maxHealth = 100;
    public Gradient gradient;
    public int currenthealth;

    public Image fill;
    public GameObject player1, player2;
    public GameObject gameoOverpanel;

    public TextMeshProUGUI winner;

    public gamecontroller gamecontroller;
    
    
    
    
    // Start is called before the first frame update
    void Start()
    {
        currenthealth = maxHealth;
        SetMaxHealth(maxHealth);
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    public void SetMaxHealth(int health)
    {// Set le slider par rapport a notre vie   
        slider.maxValue = health;
        slider.value = health;
      fill.color = gradient.Evaluate(1f);
    }
    public void SetHealth(int health)
    {// Met à jour notre slider par rapport à notre vie
        slider.value = health;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }

    public void TakeDamage(int damage)
    {// Retire de la vie et lance la fonction SetHealth
        currenthealth -= damage;
        SetHealth(currenthealth);

        if (currenthealth <=0)
        {
            Die();
            
        }
    }

    public void Die()
    {
            
        gamecontroller.manager = true;
        gamecontroller.party = true;
       // gameoOverpanel.SetActive(true);
        gameObject.SetActive(false);
        if (player1.activeSelf == false)
        {
            winner.text = "PLAYER 2 WIN !!!";
        }

        if (player2.activeSelf == false)
        {
            winner.text = "Player 1 WIN !!!";
        }
    }
}
