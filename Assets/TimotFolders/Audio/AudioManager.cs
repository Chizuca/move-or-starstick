﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{     
    public Sound[] sounds;
    void Awake()
    {// Pour chaque son dans notre array on ajoute un audiosource plus on regle le son par rapport au volume, clip etc..
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    public void Start()
    {
       
    }

    public void Play(string name)
    {// Lance le son avec ses paramètres
        Sound s =  Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }

    public void Stop(string name)
    {// Stop le son avec ses paramètres
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.source.Stop();
    }

}
