﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlayer : MonoBehaviour
{
    public float rotSpeed , objectif , variableZ;
    public KeyCode haut, bas, droite, gauche;
    public static bool malusSwitch;

    void Update()
    {
        if (Mathf.Abs(variableZ - objectif) > 180f)
        {
            //variableZ = -variableZ;
            if (variableZ < 0)
            {
                variableZ += 360f;
            }
            else
            {
                variableZ -= 360f;
            }
        }
        
        variableZ = Mathf.Lerp(objectif, variableZ, rotSpeed * Time.deltaTime);
        
        
        Quaternion rot = transform.rotation;
        //  variableZ-= Input.GetAxis("Horizontal") * rotSpeed * Time.deltaTime;
        rot = Quaternion.Euler(0, 0, variableZ);
        transform.rotation = rot;
        Vector3 pos = transform.position;
        transform.position = pos;
        if (gamecontroller.stop)
        {
            if (!malusSwitch)
            {
                if (Input.GetKey(droite) && (Input.GetKey(bas)))
                {
                    objectif = -135f;
                }
                else if (Input.GetKey(haut) && (Input.GetKey(droite)))
                {
                    objectif = -45f;
                }
                else if (Input.GetKey(bas) && (Input.GetKey(gauche)))
                {
                    objectif = 135f;
                }
                else if (Input.GetKey(gauche) && (Input.GetKey(haut)))
                {
                    objectif = 45f;
                }
                else if (Input.GetKey(haut))
                {
                    Debug.Log("Haut");
                    objectif = 0f;
                }

                else if (Input.GetKey(droite))
                {
                    Debug.Log("Droite");
                    objectif = -90f;
                    //   transform.Rotate(Vector2.right * speed * Time.deltaTime);
                }

                else if (Input.GetKey(bas))
                {
                    Debug.Log("Bas");
                    objectif = 180f;
                }

                else if (Input.GetKey(gauche))
                {
                    Debug.Log("Gauche");
                    objectif = 90f;
                    //    transform.Rotate(-Vector2.right * speed * Time.deltaTime);
                }
            }

            if (malusSwitch)
            {
                if (Input.GetKey(gauche) && (Input.GetKey(haut)))
                {
                    objectif = -135f;
                }
                else if (Input.GetKey(bas) && (Input.GetKey(gauche)))
                {
                    objectif = -45f;
                }
                else if (Input.GetKey(haut) && (Input.GetKey(droite)))
                {
                    objectif = 135f;
                }
                else if (Input.GetKey(droite) && (Input.GetKey(bas)))
                {
                    objectif = 45f;
                }
                else if (Input.GetKey(bas))
                {
                    Debug.Log("Haut");
                    objectif = 0f;
                }

                else if (Input.GetKey(gauche))
                {
                    Debug.Log("Droite");
                    objectif = -90f;
                    //   transform.Rotate(Vector2.right * speed * Time.deltaTime);
                }

                else if (Input.GetKey(haut))
                {
                    Debug.Log("Bas");
                    objectif = 180f;
                }

                else if (Input.GetKey(droite))
                {
                    Debug.Log("Gauche");
                    objectif = 90f;
                    //    transform.Rotate(-Vector2.right * speed * Time.deltaTime);
                }
            }
        }
       
    }
}
