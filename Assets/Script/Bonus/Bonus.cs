﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : MonoBehaviour
{
    public GameObject bullet1;
    public Transform firePoint;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "bonuslaser")
        {
            bullet1.SetActive(true);
            bullet1.transform.position = firePoint.position;
            bullet1.transform.rotation = firePoint.rotation;
        }
    }
}
