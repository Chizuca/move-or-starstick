﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBullet : MonoBehaviour
{
    public float speed;
    public Rigidbody2D rb1, rb2, rb3;
    public Transform firePoint;
    public GameObject bullet1, bullet2, bullet3;
    public KeyCode shoottouch;
    
    public bool canshootP1 = true;
    public bool caanshootP2 = true;
  
    void Update()
    {
       if (gamecontroller.stop)
        {
            if(Input.GetKeyDown(shoottouch))
            {
                if (bullet1.activeSelf == false)
                {
                    bullet1.SetActive(true);
                    bullet1.transform.position = firePoint.position;
                    bullet1.transform.rotation = firePoint.rotation;
                    rb1.velocity = transform.up * speed;
                }
                else if (bullet2.activeSelf == false)
                {
                    bullet2.SetActive(true);
                    bullet2.transform.position = firePoint.position;
                    bullet2.transform.rotation = firePoint.rotation;
                    rb2.velocity = transform.up * speed;
                }
                else if (bullet3.activeSelf == false)
                {
                    bullet3.SetActive(true);
                    bullet3.transform.position = firePoint.position;
                    bullet3.transform.rotation = firePoint.rotation;
                    rb3.velocity = transform.up * speed;
                }

            }
        }
      
    }
    

    void OnBecameInvisible()
    {
        bullet1.SetActive(false);
        bullet2.SetActive(false);
        bullet3.SetActive(false);
    }
    
}
