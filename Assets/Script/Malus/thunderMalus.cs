﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class thunderMalus : MonoBehaviour
{
    public static bool thunder;
    public PlayerHealth playershealthp1, playerhealthp2;

    public GameObject imagethunder;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    

// Active lE Malus
        if (thunder)
        {
            imagethunder.SetActive(true);
           
        }
        else if (thunder == false)
        {
            imagethunder.SetActive(false);
         
        }

    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        // inflige 10 de dommage aux players quand il rentre dans le collider
        if (thunder && other.collider.CompareTag("Player"))
        {
            playershealthp1.TakeDamage(10);
        }
        else if (thunder && other.collider.CompareTag("Player2"))
        {
            playerhealthp2.TakeDamage(10);
        }

    }
}


