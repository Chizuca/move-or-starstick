﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class MalusScript : MonoBehaviour
{
    public GameObject malusUI;
    public  int malusValue;
    public GameObject[] malus;            //Une list avec plusieurs Malus qui seront tirés au sort
    public bool oneTryBis;
    public float timeForRandom;
    public bool showMalus;
    public List<int> valueMalus = new List<int>(); 
    void Update()
    { 
      
        
        if (Input.GetKeyDown(KeyCode.Return) && showMalus)   //Si tu appuyes sur Entré
        {
            malus[malusValue].SetActive(false);              //Les explications du malus choisit s'enleve 
            showMalus = false;
            gamecontroller.addmalus = false;
            Time.timeScale = 1f;
            gamecontroller.stop = true;
            valueMalus.Add(PlayerPrefs.GetInt("Malus Value"));
            Debug.Log(PlayerPrefs.GetInt("Malus Value"));
            if (malusValue == 2)
            {
               // Debug.Log(BOUNCINESS.Bouciness);
                BOUNCINESS.Bouciness = true;            // Rebonds écran ++
            }
            else
            {
                BOUNCINESS.Bouciness = false;
            }

            if (malusValue == 1)
            {
                RotatePlayer.malusSwitch = true;        // Controle inverser
            }
            else
            {
                RotatePlayer.malusSwitch = false;
            }

            if (malusValue == 3)
            {
                thunderMalus.thunder = true;
            }
            else
            {
                thunderMalus.thunder = false;
            }


        }
        if (oneTryBis && !showMalus)                         //Si OneTryBis = true && showMals = false
        {
            
            malusUI.SetActive(true);                        //Alors la selection du malus s'affiche et un chrono commence
            timeForRandom += Time.deltaTime;
            if (timeForRandom > 3)                          //Quand le Chrono est supérieur à 3 
            { 
                timeForRandom = 0;                          //Alors le chrono repart à 0 et un Malus est choisit au hasard
               malusValue = Random.Range(1, 4);
               Debug.Log(malusValue);
               PlayerPrefs.SetInt("Malus Value", malusValue);
               if (valueMalus.Contains(malusValue))
               {
                   return;
               }
               oneTryBis = false;
                malusUI.SetActive(false);                   //Alors l'afffichage de sélection du Malus s'enleve et l'explication du malus apparait 
                malus[malusValue].SetActive(true);
                showMalus = true;
                gamecontroller.stop = false;
                Time.timeScale = 0f;
             
            }
            
        }

       
    }
}
