﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIScript : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public bool menuOuNon;
    public GameObject pauseMenuUI , tutorial , mainMenu , Credis;
    public string retryscene;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && menuOuNon == false)
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }
    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }
    public void TutoButton()
    {
        tutorial.SetActive(true);
        mainMenu.SetActive(false);
    }
    public void ReturnTutoButton()
    {
        tutorial.SetActive(false);
        mainMenu.SetActive(true);
    }
    public void CredisButton()
    {
        Credis.SetActive(true);
        mainMenu.SetActive(false);
    }
    public void ReturnCredisButton()
    {
        Credis.SetActive(false);
        mainMenu.SetActive(true);
    }
    public void LoadScene(int scene)
    {
        SceneManager.LoadScene(scene);
        Time.timeScale = 1f;
    }

    public void Retry()
    {
        SceneManager.LoadScene(retryscene);
    }
}
